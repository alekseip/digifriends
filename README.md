# Frontend Developer Candidate - Mock Web Project

## Quick start

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.4.
__Make sure you have Node version >= 6.0 and NPM >= 3__

To build and run the application execute following:

```bash
npm install
npm run build
```

Now you can open the dist/index.html in your favorite browser. The folder ```dist``` is ready for deployment on any server (no backend language support needed).

For running in development mode run following commands:

```bash
npm install
npm start
```
and go to [http://0.0.0.0:4200/](http://0.0.0.0:4200/) or [http://localhost:4200/](http://localhost:4200/) in your browser

## Solution description and notes

Here are the list of main used libraries/frameworks:

* __Angular 4.0__ - the last version of my favorite framework
* __Angular CLI 1.0.4__ - command line tool to initialize, develop, scaffold and maintain Angular applications
* __Typescript 2.2.0__ - TypeScript is a typed superset of JavaScript that compiles to plain JavaScript
* __Webpack__ - build system to make production codebase smaller (used by Angular CLI internally)
* __Bootstrap 3.3.7__ - My favorite ui/layout framework
* __Font Awesome 4.7.0__ - Additional set of icons
* __Karma + Jasmine__ - common (for Angular) set of unit-testing tools

### Some important notes about my solution:

All four user stories are implemented.

I have not implemented any backend. Instead I have used browser based storage (IndexedDB). It does not mean I can not do it - I just wanted to make the project simpler. With two separate parts in project it would be complicated to make it running, set up the deployment process and to write unit-tests. Ideally it should be two separate projects for backend and frontend.

If the backend part will be implemented in the future - it should not be a problem to replace the current implementation of the StorageService with the new one, which calls the real backend instead of working with the browser DB. The other way is to use current implementation of StorageService as a cache for offline mode and implement some other service to make requests for data on backend when the internet connection is available.

I did not finished all the necessary unit-tests. I have implemented only a couple of most important ones. e2e or integrations tests are also not implemented - it seems like an overkill for a such simple task.

What can be improved in current project:
* write unit-test for all user stories (ideally for all source codes)
* write more detailed help in help panel
* deleting of history entry directly from the history panel
