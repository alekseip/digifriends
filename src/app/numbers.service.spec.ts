import { TestBed, async } from '@angular/core/testing';

import { NumbersService } from './numbers.service';

describe('NumbersService', () => {

  let service : NumbersService;

  beforeEach(() => {
    const injector = TestBed.configureTestingModule({
      providers: [NumbersService]
    });
    service = injector.get(NumbersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should calculate digifriends for x=6', async(() => {
    const x = 6;
    const resultPromise = service.calculateDigifriends(x)
      .then(result => {
        expect(result).toBeDefined();
        expect(result).toContain(16);
        expect(result).toContain(21);
        expect(result).toContain(42);
      });
  }));
});
