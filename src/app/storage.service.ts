import { Injectable } from '@angular/core';
import { DigiRecord } from "./models/digi-record";
import { Subject } from 'rxjs/Subject';

@Injectable()
export class StorageService {

  private db : IDBDatabase;
  private storage : Set<DigiRecord>;
  private readTransaction : IDBTransaction;
  private writeTransaction : IDBTransaction;
  private dbOpen : boolean;
  private numbersObservableSource : Subject<DigiRecord[]>;
  public numbersObservable;
  
  constructor() {
    this.numbersObservableSource = new Subject<DigiRecord[]>();
    this.numbersObservable = this.numbersObservableSource.asObservable();
    this.initDB();
  }

  /**
   * saveNumber - saving number and its digifriends to the database
   * @param record DigiRecord record that should be saved
   */
  public saveNumber(record : DigiRecord) : Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.saveData(record, resolve, reject);
    });
  }

  /**
   * Find DigiRecord in database
   * @param x number a number for which digifriends were requested
   */
  public getNumber(x : number) : Promise<DigiRecord> {
    return new Promise((resolve, reject) => {
      this.getData(x, resolve, reject);
    });
  }

  private initDB() {
    const dbFactory = window.indexedDB;
    if (dbFactory) {
      const dbRequest = dbFactory.open('diginumbers', 2);
      dbRequest.onsuccess = event => {
        this.db = dbRequest.result;
        this.dbOpen = true;
        this.listData();
      };
      dbRequest.onupgradeneeded = event => {
        const db = (event.target as IDBOpenDBRequest).result;
        const store = db.createObjectStore('diginumbers', { keyPath: 'x' });
      };
      dbRequest.onerror = () => {
        this.dbOpen = false;
        this.storage = new Set<DigiRecord>();
      };
    } else {
      this.dbOpen = false;
      this.storage = new Set<DigiRecord>();
    }
  }

  private getData(x : number, resolve, reject) {
    if (this.dbOpen) {
      // readwrite transaction because we will update the record after reading
      const multiTransaction = this.db.transaction('diginumbers', 'readwrite');
      multiTransaction.onerror = () => {
        reject('Transaction failed!');
      };
      const store = multiTransaction.objectStore('diginumbers');
      var request = store.get(x);
      request.onerror = (event) => {
        reject('Request for data failed!');
      };
      request.onsuccess = (event) => {
        if (request.result) {
          // create record with updated touched flag
          const result = new DigiRecord(request.result.x, request.result.digifriends, new Date());
          // Create another request that inserts the item back into the database
          const updateTouchedRequest = store.put(result);
          // When this new request succeeds, resolve the initial promise with updated data
          updateTouchedRequest.onsuccess = () => {
            this.listData();
            resolve(result);
          };
        } else {
          reject('Requested data not found!');
        }
      };
    } else {
      // lookup for an element in Set
      const result = this.storage.forEach(element => {
        if (element.x == x) {
          resolve(element);
          return false;
        }
        return true;
      });
      if (result) reject('Data not found!');
    }
  }
  
  private listData() {
    let values : DigiRecord[];
    let outdatedValues : DigiRecord[];
    if (this.dbOpen) {
      values = [];
      outdatedValues = [];
      const readTransaction = this.db.transaction('diginumbers');
      readTransaction.onerror = () => {
        this.numbersObservableSource.error('Read transaction failed');
      };
      const store = readTransaction.objectStore('diginumbers');
      const dbRequest = store.openCursor();
      dbRequest.onsuccess = (event) => {
        const cursor = dbRequest.result;
        // if there is still another cursor to go, keep runing this code
        if(cursor) {
          // parse incoming value
          const digiValue = DigiRecord.parse(cursor.value);
          // check if current value is outdated
          if (digiValue.isOutdated()) {
            outdatedValues.push(digiValue);
          } else {
            values.push(digiValue);
          }
          // continue on to the next item in the cursor
          cursor.continue();
          // if there are no more cursor items to iterate through, say so, and exit the function 
        } else {
          console.log('fetching data from DB finished', values);
          this.removeData(outdatedValues, () => {
            this.numbersObservableSource.next(values);
          });
        }
      }
    } else {
      if (this.storage && this.storage.size > 0) {
        this.storage.forEach((digiValue : DigiRecord) => {
          if (digiValue.isOutdated()) {
            outdatedValues.push(digiValue);
          } else {
            values.push(digiValue);
          }
        });
      }
      console.log('received data from memory storage', values);
      this.removeData(outdatedValues, () => {
        this.numbersObservableSource.next(values);
      });
    }
  }

  private removeData(records : DigiRecord[], onsuccess) {
    if (records && records.length > 0) {
      if (this.dbOpen) {
        // open a write transaction, ready for removing the data
        const deleteTransaction = this.db.transaction(['diginumbers'], 'readwrite');
        // call onsuccess handler at the end of transaction
        deleteTransaction.oncomplete = () => {
          if (onsuccess) onsuccess();
        };
        const store = deleteTransaction.objectStore('diginumbers');
        records.forEach(record => {
          const request = store.delete(record.x);
        });
      } else {
        records.forEach(record => {
          this.storage.delete(record);
        });
        if (onsuccess) onsuccess();
      }
    } else {
      if (onsuccess) onsuccess();
    }
  }

  private saveData(record : DigiRecord, resolve, reject) {
    if (this.dbOpen) {
      // open a write transaction, ready for adding the data
      const writeTransaction = this.db.transaction(['diginumbers'], 'readwrite');
      // report on the success of opening the transaction
      writeTransaction.oncomplete = () => {
        // update the list of data on client
        this.listData();
      };
      writeTransaction.onerror = () => {
        reject('Write transaction failed');
      };
      const store = writeTransaction.objectStore('diginumbers');
      // add our newItem object to the object store
      const objectStoreRequest = store.put(record);        
      objectStoreRequest.onsuccess = (event) => {
        resolve(true);
      };
      objectStoreRequest.onerror = () => {
        reject("Error saving to DB");
      }
    } else {
      this.storage.add(record);
      this.listData();
      resolve(true);
    }
  }
}
