import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DigiRecord } from "../models/digi-record";

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent {

  constructor() {}

  private selectedNumber : number;

  // main property - the history of digifriends have been calculated
  private _records : DigiRecord[];
  @Input() set history(records : DigiRecord[]) {
    console.log('set history', records);
    if (records) {
      // sort array by touched date
      this._records = records.sort((a : DigiRecord, b : DigiRecord) => {
        // reverse date order
        if (a.touched > b.touched) return -1;
        if (a.touched < b.touched) return 1;
        return 0;
      });
    }
  };
  get history() {
    return this._records;
  }

  // output binding for selecting a history entry
  @Output() recordClick = new EventEmitter<number>();

  onRecord(record : DigiRecord) {
    this.selectedNumber = record.x;
    // propagate selected number to parent component
    if (this.recordClick) this.recordClick.emit(record.x);
  }

}
