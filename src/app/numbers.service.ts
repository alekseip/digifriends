import { Injectable } from '@angular/core';

@Injectable()
export class NumbersService {

  constructor() { }

  public calculateDigifriends(x : number) : Promise<number[]> {
    return new Promise<number[]>((resolve, reject) => {
      const digifriends = this.calculate(x);
      resolve(digifriends);
    });
  }

  public lookupDigifriend(y : number, digifriends : number[]) {
    // lookup for a friend
    return digifriends.indexOf(y) >= 0;
  }

  private calculate(x : number) : number[] {
    const digifriends : number[] = [];
    for (let i = 1; i <= x; i++) {
      const isDiv3 = (i % 3) === 0;
      const isDiv5 = (i % 5) === 0;
      if (isDiv3) digifriends.push(i + i * x);
      if (isDiv5) digifriends.push(i + i * Math.round(Math.sqrt(x)) + 1);
      if (isDiv3 && isDiv5) digifriends.push(i + i * 3 * 5 * Math.round(Math.sqrt(x)) + 2);
    }
    return digifriends;
  }
}
