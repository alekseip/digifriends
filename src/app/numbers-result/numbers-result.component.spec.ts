import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NumbersResultComponent } from './numbers-result.component';

describe('NumbersResultComponent', () => {
  let component: NumbersResultComponent;
  let fixture: ComponentFixture<NumbersResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumbersResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumbersResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
