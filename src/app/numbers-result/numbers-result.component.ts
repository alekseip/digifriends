import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-numbers-result',
  templateUrl: './numbers-result.component.html',
  styleUrls: ['./numbers-result.component.scss']
})
export class NumbersResultComponent {

  constructor() { }

  // digifriends to show as a result
  @Input() digifriends : number[];
  
  // three-state flag to show a message
  @Input() digifriendFound : boolean | null;
}
