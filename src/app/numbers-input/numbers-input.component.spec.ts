import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule }   from '@angular/forms';
import { NumbersInputComponent } from './numbers-input.component';

describe('NumbersInputComponent', () => {
  let component: NumbersInputComponent;
  let fixture: ComponentFixture<NumbersInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [ NumbersInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NumbersInputComponent);
    component = fixture.componentInstance;
    //component.digifriendFound = false;
    //component.selectedX = undefined;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
