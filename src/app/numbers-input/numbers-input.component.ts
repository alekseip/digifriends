import { Component, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-numbers-input',
  templateUrl: './numbers-input.component.html',
  styleUrls: ['./numbers-input.component.scss']
})
export class NumbersInputComponent {

  // data model
  public model = {
    inputY: null,
    inputZ: null,
  };

  // input flag to colorize Y if it was found
  @Input() digifriendFound : boolean | null;

  // input flag to colorize Y if it was found
  @Input() set selectedX(x : number) {
    if (x !== undefined) {
      console.log('external X given:', x);
      this.model.inputZ = x;
    }
  };

  // output binding for calculate button
  @Output() calculateClick = new EventEmitter<number>();

  // output binding for check button
  @Output() checkClick = new EventEmitter<any>();

  constructor() { }

  onCalculate() {
    if (this.calculateClick) this.calculateClick.emit(this.model.inputZ);
  }

  onCheck() {
    if (this.checkClick) this.checkClick.emit({y : this.model.inputY, z : this.model.inputZ});
  }

}
