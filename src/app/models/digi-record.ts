// constant representing the timeout for digiRecord to be stored in DB in days
const timeout = 30;

/**
 * class represents a record in the database
 */
export class DigiRecord {
  constructor(public x : number, public digifriends : number[], public touched : Date) {}
  // class builder (parses the data from DB)
  static parse(value : {x : number, digifriends : number[], touched : Date}) : DigiRecord {
    return new DigiRecord(value.x, value.digifriends, value.touched);
  }
  // check if the time for storing this value is expired
  isOutdated() : boolean {
    const today = new Date();
    const expireDate = this.touched;
    expireDate.setDate(expireDate.getDate() + timeout);
    return  today > expireDate;
  }
}
