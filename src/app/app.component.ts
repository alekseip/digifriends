import { Component, OnInit } from '@angular/core';
import { NumbersService } from "./numbers.service";
import { StorageService } from "./storage.service";
import { DigiRecord } from "./models/digi-record";
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-root',
  providers: [NumbersService, StorageService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  // header nav panel UI control parameters
  needHelp: boolean = false;
  isCollapsed: boolean = true;
  // digifriends for current number
  currentDigifriends: number[] = null;
  // history
  history: DigiRecord[];
  // loaded flag
  loaded: boolean = false;
  // shows if given Y was found in digifriends of Z
  digifriendFound: boolean | null = null;
  // X value selected from history
  selectedX : number;

  constructor(private numbersService: NumbersService, private storageService: StorageService) { }

  ngOnInit() {
    // subscribe to history changes
    this.storageService.numbersObservable
      .subscribe(history => {
        this.history = history;
        this.loaded = true;
      });
  }

  // calculate click handler
  onCalculateClick(x: number) {
    this.findRecord(x)
      .then((record : DigiRecord) => {
        // show the result to the user
        this.currentDigifriends = record.digifriends;
      });
  }

  // check click handler
  onCheckClick(data : {y : number, z : number}) {
    const {y, z} = data;
    this.findRecord(z)
      .then((record : DigiRecord) => {
        // check if given number is in the digifriends array
        this.digifriendFound = this.numbersService.lookupDigifriend(y, record.digifriends);
      });
  }

  // select the value in history handler
  onHistorySelect(x : number) {
    console.log('onHistorySelect:', x);
    this.findRecord(x)
      .then((record : DigiRecord) => {
        // show the result to the user
        this.currentDigifriends = record.digifriends;
        // propagate the given X to the input form
        this.selectedX = x;
      });
  }

  private findRecord(x : number) : Promise<DigiRecord> {
    this.loaded = false;
    return new Promise((resolve, reject) => {
    // check if the number was already calculated
    // we are requesting the database because this way
    // we will update the touched flag on the record if it was found
    this.storageService.getNumber(x)
      .then((record: DigiRecord) => {
        this.loaded = true;
        resolve(record);
      }, () => {
        // record was not found - calculate
        // calculate digifriends for given number
        this.numbersService.calculateDigifriends(x)
          .then((digifriends: number[]) => {
            const record = new DigiRecord(x, digifriends, new Date());
            // save result to the database
            this.storageService.saveNumber(record)
              .then((isSucceeded: boolean) => {
                resolve(record);
                this.loaded = true;
              }, reject);
          }, reject);
      });
    });
  }
}
