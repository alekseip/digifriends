import { TestBed, async } from '@angular/core/testing';
import { FormsModule }   from '@angular/forms';
import { AppComponent } from './app.component';
import { NumbersInputComponent } from './numbers-input/numbers-input.component';
import { NumbersResultComponent } from './numbers-result/numbers-result.component';
import { HistoryComponent } from './history/history.component';
import { NumbersService } from "./numbers.service";
import { StorageService } from "./storage.service";
import { DigiRecord } from "./models/digi-record";

describe('AppComponent', () => {

  let fixture;
  let app : AppComponent;
  let numbersService : NumbersService;
  let storageService : StorageService;
  let getNumberSpy;
  const testDigiRecord : DigiRecord = new DigiRecord(6, [16, 21, 42], new Date());

  beforeEach(async(() => {
    // configure the tested component
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        NumbersInputComponent,
        NumbersResultComponent,
        HistoryComponent,
        AppComponent
      ],
      providers: [
        NumbersService,
        StorageService
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    // initialize variables
    // test wrapper
    fixture = TestBed.createComponent(AppComponent);
    // component instance
    app = fixture.debugElement.componentInstance;
    // services to spy on
    numbersService = fixture.debugElement.injector.get(NumbersService);
    storageService = fixture.debugElement.injector.get(StorageService);
  });

  it('should create the app', async(() => {
    expect(app).toBeTruthy();
  }));

  it('should get the digifriends from DB (User story #1)', async(() => {
    // test initial value
    expect(app.currentDigifriends).toBeNull();
    // suppose we want to get the number from DB
    getNumberSpy = spyOn(storageService, 'getNumber')
          .and.returnValue(Promise.resolve(testDigiRecord));
    // emulate click
    app.onCalculateClick(testDigiRecord.x);
    fixture.detectChanges();
    fixture.whenStable().then(() => { // wait for async getNumber
      fixture.detectChanges();
      expect(getNumberSpy).toHaveBeenCalledWith(testDigiRecord.x);
      expect(app.currentDigifriends).toEqual(testDigiRecord.digifriends);
    });
  }));

  it('should calculate it if it is not saved in DB (User story #1)', async(() => {
    // test initial value
    expect(app.currentDigifriends).toBeNull('initial state of currentDigifriends should be null');
    // suppose we want to receive a rejection - no such number in DB
    getNumberSpy = spyOn(storageService, 'getNumber')
      .and.returnValue(Promise.reject('No such number in DB'));
    // setup spy for next call to NumbersService
    const calculateSpy = spyOn(numbersService, 'calculateDigifriends')
      .and.returnValue(Promise.resolve(testDigiRecord.digifriends));
    // prevent saving state
    const saveSpy = spyOn(storageService, 'saveNumber')
      .and.returnValue(Promise.resolve(true));
    // emulate click
    app.onCalculateClick(testDigiRecord.x);
    fixture.detectChanges();
    fixture.whenStable().then(() => { // wait for async
      // expect the storage was called for number
      expect(getNumberSpy).toHaveBeenCalledWith(testDigiRecord.x);
      // expect the number service was called to calculate it
      expect(calculateSpy).toHaveBeenCalledWith(testDigiRecord.x);
      // expect storage was called to save the results
      expect(saveSpy).toHaveBeenCalled();
      fixture.detectChanges();
      // wait for resolving the final promise
      fixture.whenStable().then(() => {
        // expect the results were shown to the user
        expect(app.currentDigifriends).toEqual(testDigiRecord.digifriends, 'final results must be shown to the user');
        expect(app.loaded).toBeTruthy('loading workflow should come to the end');
      });
    });
  }));
});
