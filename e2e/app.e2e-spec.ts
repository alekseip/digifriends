import { DigifriendsPage } from './app.po';

describe('digifriends App', () => {
  let page: DigifriendsPage;

  beforeEach(() => {
    page = new DigifriendsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
